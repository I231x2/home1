import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Balls {

   enum Color {green, red};
   
   public static void main (String[] param) {
      // for debugging
	   Color[] pallid = {Color.green, Color.green, Color.red, Color.red, Color.green, Color.red};
	   reorder(pallid);
	   for (Color a : pallid){
		   System.out.println(a);
	   }
	   System.out.println();
	   
	   reorder(pallid);
	   for (Color a : pallid){
		   System.out.println(a);
	   }
   }
   
   public static void reorder (Color[] balls) {
      // TODO!!! Your program here
	   
	   int r = 0;
	   for(Color kontrollitav : balls){ 
		   if(kontrollitav == Color.red) r++;
	   }
	   if(r != 0 && r != balls.length){
		   for(int i = 0; i < balls.length; i++){
			   if(i < r) balls[i] = Color.red;
			   else balls[i] = Color.green;
		   }
	   }
   }
   
}

